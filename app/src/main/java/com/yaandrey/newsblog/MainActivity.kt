package com.yaandrey.newsblog

import android.os.Bundle
import com.yaandrey.newsblog.common.BaseMvpActivity

class MainActivity: BaseMvpActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}