package com.yaandrey.newsblog.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.yaandrey.newsblog.R
import kotlinx.android.synthetic.main.item_network_state.view.*

class LoadStateViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val buttonRepeat = view.button_repeat
    private val errorMsg = view.message_error
    private val progressBar = view.progress_bar

    fun onBind(loadState: LoadState, clickListener: NewsAdapter.OnClickItemListener) {

        if (loadState is LoadState.Error) {
            errorMsg.text = loadState.error.localizedMessage
        }
        progressBar.isVisible = loadState is LoadState.Loading
        buttonRepeat.isVisible = loadState !is LoadState.Loading
        errorMsg.isVisible = loadState !is LoadState.Loading

        buttonRepeat.setOnClickListener { clickListener.retry() }
    }

    companion object {
        fun create(parent: ViewGroup): LoadStateViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.item_network_state, parent, false)
            return LoadStateViewHolder(view)
        }
    }
}