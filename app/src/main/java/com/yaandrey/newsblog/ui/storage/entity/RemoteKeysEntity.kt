package com.yaandrey.newsblog.ui.storage.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yaandrey.newsblog.storage.enitity.MainEntity

@Entity(tableName = "remote_keys")
data class RemoteKeysEntity(
    @PrimaryKey
    val uid: String,
    val prevKey: Int?,
    val nextKey: Int?
) : MainEntity()