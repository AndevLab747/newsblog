package com.yaandrey.newsblog.ui.storage.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yaandrey.newsblog.storage.enitity.MainEntity

@Entity(tableName = "news")
data class NewsEntity(
    @PrimaryKey
    val uid: String,
    val title: String,
    val description: String,
    val imageUrl: String,
    val url: String,
    val publicationDate: String
): MainEntity()