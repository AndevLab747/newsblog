package com.yaandrey.newsblog.ui.di

import android.content.Context
import com.yaandrey.newsblog.api.Api
import com.yaandrey.newsblog.di.ScreenScope
import com.yaandrey.newsblog.domain.repository.DataBaseRepositoryImpl
import com.yaandrey.newsblog.domain.repository.NetworkRepositoryImpl
import com.yaandrey.newsblog.domain.repository.SharedPreferenceRepositoryImpl
import com.yaandrey.newsblog.domain.repository.network.NetworkRepository
import com.yaandrey.newsblog.domain.repository.prefs.SharedPreferenceRepository
import com.yaandrey.newsblog.domain.repository.storage.DataBaseRepository
import com.yaandrey.newsblog.storage.AppDataBase
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @ScreenScope
    @Provides
    fun provideDataBaseRepository(appDataBase: AppDataBase): DataBaseRepository =
        DataBaseRepositoryImpl(appDataBase)

    @ScreenScope
    @Provides
    fun provideNetworkRepository(api: Api): NetworkRepository = NetworkRepositoryImpl(api)

    @ScreenScope
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferenceRepository =
        SharedPreferenceRepositoryImpl(context)

}