package com.yaandrey.newsblog.ui.storage.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yaandrey.newsblog.ui.storage.entity.NewsEntity

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNews(news: List<NewsEntity>)

    @Query("select * from news")
    fun getNews(): PagingSource<Int, NewsEntity>

    @Query("delete from news")
    suspend fun clearNews()

}