package com.yaandrey.newsblog.ui.data

import com.yaandrey.newsblog.api.dto.NewsDto
import com.yaandrey.newsblog.data.Mapper
import com.yaandrey.newsblog.data.NewsMapper
import com.yaandrey.newsblog.data.NewsToListModel
import com.yaandrey.newsblog.ui.domain.model.NewsModel
import com.yaandrey.newsblog.ui.storage.entity.NewsEntity
import java.util.*
import kotlin.collections.ArrayList


class ToListModelMapper : NewsMapper<NewsModel>() {
    override fun map(from: NewsDto): List<NewsModel> {
        val resultList = ArrayList<NewsModel>()
        from.articles.forEach {
            resultList.add(
                NewsModel(
                    UUID.randomUUID().toString(),
                    it.title ?: "",
                    it.description ?: "",
                    it.urlToImage ?: "",
                    it.url ?: "",
                    it.publishedAt ?: ""
                )
            )
        }
        return resultList
    }
}

class ToListEntityFromModelMapper : NewsToListModel<NewsModel, NewsEntity>() {
    override fun map(from: List<NewsModel>): List<NewsEntity> {
        val resultList = ArrayList<NewsEntity>()
        from.forEach { resultList.add(NewsEntity(it.uid, it.title, it.description, it.imageUrl, it.url, it.publicationDate)) }
        return resultList
    }
}

class ToListModelFromEntityMapper : NewsToListModel<NewsEntity, NewsModel>() {
    override fun map(from: List<NewsEntity>): List<NewsModel> {
        val result = ArrayList<NewsModel>()
        from.forEach {
            result.add(
                NewsModel(
                    it.uid,
                    it.title,
                    it.description,
                    it.imageUrl,
                    it.url,
                    it.publicationDate
                )
            )
        }
        return result
    }

}

class NewsToEntityMapper : Mapper<NewsModel, NewsEntity>() {
    override fun map(from: NewsModel): NewsEntity {
        return from.run { NewsEntity(uid, title, description, imageUrl, url, publicationDate) }
    }
}

class ToModelFromEntityMapper : Mapper<NewsEntity, NewsModel>() {
    override fun map(from: NewsEntity): NewsModel {
        return from.run { NewsModel(uid, title, description, imageUrl, url, publicationDate) }
    }
}