package com.yaandrey.newsblog.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import com.yaandrey.newsblog.App
import com.yaandrey.newsblog.R
import com.yaandrey.newsblog.common.BaseMvpFragment
import com.yaandrey.newsblog.ui.adapter.LoadStateAdapter
import com.yaandrey.newsblog.ui.adapter.NewsAdapter
import com.yaandrey.newsblog.ui.data.ToModelFromEntityMapper
import com.yaandrey.newsblog.ui.di.MainComponent
import com.yaandrey.newsblog.ui.domain.model.NewsModel
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalPagingApi
class MainFragment : BaseMvpFragment(), MainView, CoroutineScope by MainScope() {

    @Inject
    lateinit var presenter: MainPresenter

    private lateinit var pagingAdapter: NewsAdapter

    private var loadJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        MainComponent
            .init((requireActivity().application as App).appComponent)
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pagingAdapter = NewsAdapter(clickListener)
        initRecyclerView()
        load()
    }

    private val clickListener = object : NewsAdapter.OnClickItemListener {
        override fun onClickItem(item: NewsModel) {
            val bundle = Bundle()
            bundle.putString("URL", item.url)
            findNavController().navigate(R.id.action_mainFragment_to_webFragment, bundle)
        }

        override fun retry() {
            pagingAdapter.retry()
        }
    }

    private fun initRecyclerView() {
        recycler_view_news.adapter = pagingAdapter.withLoadStateHeaderAndFooter(
            header = LoadStateAdapter(clickListener),
            footer = LoadStateAdapter(clickListener)
        )
        pagingAdapter.addLoadStateListener { loadState ->

            recycler_view_news.isVisible = loadState.refresh is LoadState.NotLoading
            main_progress_bar.isVisible = loadState.refresh is LoadState.Loading
            retry_button.isVisible = loadState.refresh is LoadState.Error

            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                showDialog("Error", "\uD83D\uDE28 Wooops ${it.error}")
            }
        }
        retry_button.setOnClickListener { pagingAdapter.retry() }
    }

    private fun load() {
        loadJob?.cancel()
        loadJob = lifecycleScope.launch {
            presenter.loadNews().collectLatest {pagingData ->
                val result = pagingData.map { ToModelFromEntityMapper().map(it) }
                pagingAdapter.submitData(result)
            }
        }
    }
}