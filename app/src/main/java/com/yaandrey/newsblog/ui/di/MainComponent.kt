package com.yaandrey.newsblog.ui.di

import androidx.paging.ExperimentalPagingApi
import com.yaandrey.newsblog.di.AppComponent
import com.yaandrey.newsblog.di.ScreenScope
import com.yaandrey.newsblog.ui.MainFragment
import dagger.Component

@ScreenScope
@Component(modules = [MainModule::class], dependencies = [AppComponent::class])
interface MainComponent {

    @ExperimentalPagingApi
    fun inject(mainFragment: MainFragment)

    @Component.Builder
    interface Builder {
        fun appComponent(appComponent: AppComponent): Builder
        fun build(): MainComponent
    }

    companion object {
        fun init(appComponent: AppComponent): MainComponent {
            return DaggerMainComponent.builder()
                .appComponent(appComponent)
                .build()
        }
    }
}