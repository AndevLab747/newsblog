package com.yaandrey.newsblog.ui

import com.yaandrey.newsblog.common.BaseMvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface MainView : BaseMvpView {
}