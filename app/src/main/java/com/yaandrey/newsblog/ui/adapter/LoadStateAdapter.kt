package com.yaandrey.newsblog.ui.adapter

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

class LoadStateAdapter(private val clickItemListener: NewsAdapter.OnClickItemListener) : LoadStateAdapter<LoadStateViewHolder>() {

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) = holder.onBind(loadState, clickItemListener)

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState)
            = LoadStateViewHolder.create(parent)
}