package com.yaandrey.newsblog.ui.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yaandrey.newsblog.ui.storage.entity.RemoteKeysEntity

@Dao
interface RemoteKeysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKeys : List<RemoteKeysEntity>)

    @Query("select * from remote_keys where uid = :id")
    suspend fun remoteKeysNewsId(id: String): RemoteKeysEntity?

    @Query("delete from remote_keys")
    suspend fun cleatRemoteKeys()
}