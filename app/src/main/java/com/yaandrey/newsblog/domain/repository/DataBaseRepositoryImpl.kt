package com.yaandrey.newsblog.domain.repository

import androidx.paging.PagingSource
import com.yaandrey.newsblog.domain.repository.storage.DataBaseRepository
import com.yaandrey.newsblog.storage.AppDataBase
import com.yaandrey.newsblog.ui.storage.entity.NewsEntity
import javax.inject.Inject

class DataBaseRepositoryImpl @Inject constructor(private val db: AppDataBase) :
    DataBaseRepository {

    override suspend fun insert(list: List<NewsEntity>) {
        return db.newsDao().insertNews(list)
    }

    override fun getNews(): PagingSource<Int, NewsEntity> {
        return db.newsDao().getNews()
    }

    override fun getAppDataBase(): AppDataBase {
        return db
    }

}