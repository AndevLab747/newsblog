package com.yaandrey.newsblog.domain.repository

import android.content.Context
import androidx.preference.PreferenceManager
import com.yaandrey.newsblog.domain.repository.prefs.SharedPreferenceRepository
import com.yaandrey.newsblog.utils.int
import javax.inject.Inject

class SharedPreferenceRepositoryImpl @Inject constructor(context: Context) :
    SharedPreferenceRepository {
    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    override var page: Int by prefs.int()
}