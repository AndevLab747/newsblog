package com.yaandrey.newsblog.domain.repository.network

import com.yaandrey.newsblog.api.dto.NewsDto

interface NetworkRepository {
    fun getNewsPageOne(page: Int): NewsDto?
}