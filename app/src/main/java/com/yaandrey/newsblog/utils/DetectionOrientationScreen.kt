package com.yaandrey.newsblog.utils

import android.content.Context
import android.content.res.Configuration

object DetectionOrientationScreen {

    fun isPortrait(context: Context): Boolean {
        val applicationContext = context.applicationContext
        val configuration = applicationContext.resources.configuration
        return configuration.orientation == Configuration.ORIENTATION_PORTRAIT
    }

    fun isLandscape(context: Context): Boolean {
        val applicationContext = context.applicationContext
        val configuration = applicationContext.resources.configuration
        return configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
    }
}