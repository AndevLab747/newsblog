package com.yaandrey.newsblog.di

import android.content.Context
import androidx.room.Room
import com.yaandrey.newsblog.storage.AppDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun provideAppDataBase(context: Context): AppDataBase =
        Room.databaseBuilder(context, AppDataBase::class.java, "newsBlodDB")
            .build()
}