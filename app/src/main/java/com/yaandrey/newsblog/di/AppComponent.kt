package com.yaandrey.newsblog.di

import android.content.Context
import com.yaandrey.newsblog.App
import com.yaandrey.newsblog.api.Api
import com.yaandrey.newsblog.storage.AppDataBase
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [NetworkModule::class, StorageModule::class])
@Singleton
interface AppComponent {

    fun context(): Context
    fun api(): Api
    fun appDataBase(): AppDataBase

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun context(context: Context): Builder
    }
}