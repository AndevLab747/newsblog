package com.yaandrey.newsblog.di

import com.yaandrey.newsblog.BASE_URL
import com.yaandrey.newsblog.api.Api
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


const val API_KEY = "26eddb253e7840f988aec61f2ece2907"

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideApi(): Api {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(provideHttpClient())
            .build().create(Api::class.java)
    }

    private fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                chain.proceed(chain
                    .request().run {
                        val url = url
                            .newBuilder()
                            .addQueryParameter("apiKey", API_KEY)
                            .build()

                        newBuilder()
                            .url(url)
                            .build()
                    }
                )
            }
            .apply {
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                    addInterceptor(this)
                }
            }
            .build()
    }
}